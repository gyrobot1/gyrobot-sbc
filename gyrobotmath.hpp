#ifndef GYROBOTMATH_HPP
#define GYROBOTMATH_HPP

inline float lerp(float v1, float v2, float l){
	return v1+l*(v2-v1);
}

inline double clamp(double v, double min, double max){
	return qMax(min, qMin(v, max));
}

#endif // GYROBOTMATH_HPP
