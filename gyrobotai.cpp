#include "gyrobotai.h"
#include "gyrobotmath.hpp"
#include <QDebug>
#include <QtMath>

GyrobotAI::GyrobotAI(QObject *parent)  : QObject(parent)
{
	PID * gains = m_balance_regulator.getGains();
	gains->proportional= 300.0;
	gains->integrational = 600.0;
	gains->differential = 5.0;
	m_balance_regulator.setTarget(m_pivotAngle);
	m_balance_regulator.setDiffLimit(255.0);
	gains = m_speed_regulator.getGains();
	gains->proportional = 0.02;
	gains->integrational = 0.02;
	gains->differential = 0.001;
	m_speed_regulator.setTarget(0);
	m_speed_regulator.setDiffLimit(0.3);
	QTimer *balance_timer = new QTimer(this);
	balance_timer->start(m_delta_time * 1000);
	connect(balance_timer, SIGNAL(timeout()), this, SLOT(onBalanceTime()));
}

double t = 0;
void GyrobotAI::onBalanceTime(){
	t+= 0.01;
	double v = m_speed_regulator.regulate(sin(t));
    //qInfo() << v;
	switch (m_state){
		case AIState::Calibrating:

			break;
		case AIState::Balancing:
			regulate();
			break;
	}
}

const QVector3D XDIR(1,0,0);
void GyrobotAI::regulate()
{
	if (m_state != AIState::Balancing) return;
	QVector3D rot = 50*(m_gyro+m_gyro_offset)*m_delta_time;
	QVector3D down = m_acceleration.normalized();
	QVector3D forward = QVector3D::crossProduct(XDIR, down).normalized();
	QVector3D right = QVector3D::crossProduct(down, forward).normalized();
	QQuaternion gyro_quat = QQuaternion::fromEulerAngles(rot).normalized();
	QQuaternion acc_quat = QQuaternion::fromAxes(right, -down, -forward).normalized();

	m_orientation *= gyro_quat;
	m_orientation = QQuaternion::slerp(m_orientation, acc_quat, 0.003);
	m_orientation.normalize();

	QVector3D xAxis, yAxis, zAxis;
	m_orientation.getAxes(&xAxis, &yAxis, &zAxis);
	double angle = qAtan2(yAxis.z(), yAxis.y());
    double force_feedback = m_balance_regulator.regulate(angle);
	emit regulation(-force_feedback, force_feedback);
	double speedSign = m_currentSpeed > 0 ? 1.0 : -1.0;
	double doubt = 1/abs(m_currentSpeed);
	if (doubt > 2.0){
		double dd = (angle-m_pivotAngle);
		//qInfo() << dd;
		speedSign = dd > 0 ? 1.0 : -1.0;
	}

	//m_pulseSpeed = lerp(m_pulseSpeed, speed,0.1);

	m_currentSpeed = lerp(m_currentSpeed, m_pulseSpeed, 0.05) * speedSign;
	m_position_z += m_currentSpeed*m_delta_time;


	double speed_feedback = m_speed_regulator.regulate(m_currentSpeed);
	//m_balance_regulator.setTarget(m_pivotAngle-speed_feedback);
	qInfo() << "speed_feedback: " << speed_feedback;
	qInfo() << "m_currentSpeed: " << m_currentSpeed;
}


void GyrobotAI::onInfo(QString info){
	m_state = AIState::Calibrating;
	qInfo() << info;
}

void GyrobotAI::onError(QString error){
	m_state = AIState::Calibrating;
	qInfo() << error;
}

void GyrobotAI::onSensorData(SensorData sensor_data)
{
	//qInfo("onSensorData");
	m_state = AIState::Balancing;
	m_sensor_data = sensor_data;
	m_acceleration.setY(-sensor_data.acc_x);
	m_acceleration.setZ(sensor_data.acc_z+m_balance_offset);
	m_gyro.setX(-sensor_data.gyro_y);
	m_pulseSpeed =  m_wheelCircomference* m_dataFrequency*sensor_data.speed_pulses/ m_pulsesPerRotation;


}


