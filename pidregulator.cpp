#include "pidregulator.h"
#include "gyrobotmath.hpp"
#include <QDebug>

PIDRegulator::PIDRegulator(QObject *parent)
	: QObject{parent}
{

}

double PIDRegulator::regulate(double input) {
	double difference, proportional_feedback, integrational_feedback, total_feedback;
	difference = m_target - input;
	proportional_feedback = difference*m_gains.proportional;
	integrational_feedback = (difference - m_last_difference)*m_gains.integrational;
	m_differential_feedback += difference*m_gains.differential;
	m_differential_feedback = clamp(m_differential_feedback, -m_differenctial_limit, m_differenctial_limit);
	total_feedback = proportional_feedback + integrational_feedback + m_differential_feedback;
	m_last_difference = difference;
	return total_feedback;
}

void PIDRegulator::setTarget(double value)
{
	m_target = value;
}

void PIDRegulator::setDiffLimit(double value)
{
	m_differenctial_limit = value;
}

PID *PIDRegulator::getGains()
{
	return &m_gains;
}
