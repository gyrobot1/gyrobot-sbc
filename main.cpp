#include "gyrobotai.h"
#include "serialhandler.h"

#include <QCoreApplication>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	SerialHandler *sh = new SerialHandler(&a);
	GyrobotAI *gai = new GyrobotAI(&a);
	QObject::connect(sh, SIGNAL(info(QString)), gai, SLOT(onInfo(QString)));
	QObject::connect(sh, SIGNAL(error(QString)), gai, SLOT(onError(QString)));
	QObject::connect(sh, SIGNAL(sensorData(SensorData)), gai, SLOT(onSensorData(SensorData)));
	QObject::connect(gai, SIGNAL(regulation(double,double)), sh, SLOT(onRegulation(double,double)));
	sh->startSerial("/dev/ttyACM0");
	return a.exec();
}
