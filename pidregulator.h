#ifndef PIDREGULATOR_H
#define PIDREGULATOR_H

#include <QObject>

struct PID {
	double proportional;
	double integrational;
	double differential;
};

class PIDRegulator : public QObject
{
	Q_OBJECT
	PID m_gains;
	double m_differential_feedback = 0;
	double m_last_difference = 0;
	double m_target;
	double m_differenctial_limit = 1.0;

public:
	explicit PIDRegulator(QObject *parent = nullptr);
	double regulate(double input);
	void setTarget(double value);
	void setDiffLimit(double value);
	PID * getGains();

signals:

};

#endif // PIDREGULATOR_H
