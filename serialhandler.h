#ifndef SERIALHANDLER_H
#define SERIALHANDLER_H

#include <QThread>
#include <QMutex>
#include <QByteArray>
#include <QTimer>

enum ParserState {
	Start,
	Data,
	End
};

struct SensorData {
	float acc_x;
	float acc_y;
	float acc_z;
	float gyro_x;
	float gyro_y;
	float gyro_z;
	uint8_t speed_pulses;
};

class SerialHandler : public QThread
{
	Q_OBJECT
	QString m_port;
	QMutex m_mutex_read;
	QMutex m_mutex_write;
	QByteArray m_readData;
	QByteArray m_writeData;
	QByteArray m_messageData;
	bool m_messageEnded = false;
	SensorData m_sensorData;
	QTimer m_readTimer;
	//int m_timercounter = 0;
	uint m_dataCounter = 0;
	//int m_dataFrequencyCounter = 0;
	bool m_quit = false;
	uint8_t m_parserDataType;
	int sensor_data_size = 25;// sizeof(SensorData);
	ParserState m_state = ParserState::End;

public:
	SerialHandler(QObject *parent);
	void startSerial(const QString &port);

private:
	void run() override;

	void parse(uint8_t dataByte);
	void parseData(uint8_t dataByte);
	void parseInfo(uint8_t dataByte);
	void parseSensorData(uint8_t dataByte);
signals:
	void wheelData(uint8_t speed);
	void info(const QString &msg);
	void error(const QString &e);
	void timeout(const QString &e);
	void sensorData(SensorData sensorData);

private slots:
	void onReadDataTimeout();

public slots:
	void onRegulation(double left, double right);

};

#endif // SERIALHANDLER_H
