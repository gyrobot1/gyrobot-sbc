#include "serialhandler.h"
#include <QSerialPort>
#include <QDebug>
#include <QCoreApplication>



#define STARTBYTE 170 // 10101010
#define ENDBYTE 85    // 01010101
#define ERRORTYPEBYTE 1
#define SENSORDATATYPEBYTE 2
#define INFOTYPEBYTE 3
#define MOTORREGULATIONTYPEBYTE 4

SerialHandler::SerialHandler(QObject *parent) : QThread(parent)
{
	m_readTimer.start(1);
	connect(&m_readTimer, SIGNAL(timeout()), this, SLOT(onReadDataTimeout()));
}

void SerialHandler::startSerial(const QString &port)
{
	m_port = port;
	if (!isRunning()){
		start();
	}
}

void SerialHandler::run()
{
	QSerialPort serial;
	serial.setBaudRate(115200);
	serial.setPortName(m_port);
	if (serial.open(QIODevice::ReadWrite)){
		emit info("Serial port opened");
	} else {
		qInfo("Serial port not opened.");
	}
	while (!m_quit){
		if (serial.waitForReadyRead(1)) {
			m_mutex_read.lock();
			m_readData += serial.readAll();
			m_mutex_read.unlock();
		}
		m_mutex_write.lock();
		QByteArray writeData = m_writeData;
		m_writeData.clear();
		m_mutex_write.unlock();
		if (writeData.length() > 0){
			serial.write(writeData);
			if (serial.waitForBytesWritten(1)){
				writeData.clear();
			} else {
				emit timeout("Serial write response timeout");
			}
		}
	}
}

void SerialHandler::parseInfo(uint8_t dataByte){
	switch (m_dataCounter) {
		case 0:
			m_messageData.clear();
			m_messageEnded = false;
			[[fallthrough]];
		default:
			if (m_messageEnded) {
				if (dataByte == ENDBYTE){
					m_state = ParserState::End;
					if (m_parserDataType == INFOTYPEBYTE) emit info("Arduino: " + QString(m_messageData));
					else emit error("Arduino: " + QString(m_messageData));
				}
			} else {
				m_messageData += dataByte;
			}
			if (dataByte == 0) m_messageEnded = true;
			break;
	}
	m_dataCounter++;
}

void SerialHandler::parseSensorData(uint8_t dataByte){
	uint8_t *sd  = (uint8_t *)&m_sensorData;
	if (m_dataCounter >= sensor_data_size){
		if (dataByte == ENDBYTE){
			m_state = ParserState::End;
			emit sensorData(m_sensorData);
		} else {
			qInfo() << "sensor data to long " << sensor_data_size << " > " << m_dataCounter;
		}

	} else {
		sd[m_dataCounter] = dataByte;
	}
	m_dataCounter++;
}

void SerialHandler::parseData(uint8_t dataByte){
	switch (m_parserDataType) {
	case ERRORTYPEBYTE:
	case INFOTYPEBYTE:
		parseInfo(dataByte);
		break;
	case SENSORDATATYPEBYTE:
		parseSensorData(dataByte);
		break;
	}
}

void SerialHandler::parse(uint8_t dataByte){
	switch(m_state){
		case ParserState::End:
			//qInfo()<< "ParserState::End";
			if(dataByte == STARTBYTE){
				m_state = ParserState::Start;
				m_dataCounter = 0;
			}
			break;
		case ParserState::Start:
			//qInfo()<< "ParserState::Start";
			m_parserDataType = dataByte;
			m_state = ParserState::Data;
			break;
		case ParserState::Data:
			parseData(dataByte);
			break;
	}
}

void SerialHandler::onReadDataTimeout()
{
	m_mutex_read.lock();
	for (int i = 0; i < m_readData.length(); i++){
		parse(m_readData[i]);
	}
	m_readData.clear();
	m_mutex_read.unlock();
}

void SerialHandler::onRegulation(double left, double right)
{
	uint8_t dir_byte = left < 0 ? 1:0;
	dir_byte += right < 0 ? 2:0;
	left = qMin(255.0, abs(left));
	right = qMin(255.0, abs(right));
	m_mutex_write.lock();
	m_writeData += STARTBYTE;
	m_writeData += MOTORREGULATIONTYPEBYTE;
	m_writeData += (uint8_t)dir_byte;
	m_writeData += (uint8_t)left;
	m_writeData += (uint8_t)right;
	m_writeData += ENDBYTE;
	m_mutex_write.unlock();
}
