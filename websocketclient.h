#ifndef WEBSOCKETCLIENT_H
#define WEBSOCKETCLIENT_H

#include <QObject>
#include <QTimer>
#include <QWebSocket>

class WebSocketClient : public QObject
{
	Q_OBJECT
	QWebSocket m_webSocket;
	QUrl m_url;
	bool m_debug;
	QString m_id;
	QStringList m_clients;
	QTimer m_pingTimer;

public:
	explicit WebSocketClient(const QUrl &url, bool debug, QObject *parent = nullptr);
	void sendAll(const QJsonObject &obj);
	void sendTo(const QString &uuid, const QJsonObject &data);
	void changeSector(const QString &id);
	void createSector(const QString &sectorId, const QString &hostId, const QString &hostToken);
private slots:
	void onConnected();
	void onTextMessageReceived(QString message);
	void onClosed();
	void onError(QAbstractSocket::SocketError error);
	void onPingTimer();
private:
	void handleMessage(const QJsonObject &json);
	void handleInit(const QJsonObject &json);
	void handleClientAdded(const QJsonObject &json);
	void handleClientRemoved(const QJsonObject &json);
	void sendMessage(QString msg);
	void handleClientMessage(const QJsonObject &json);
	void handleError(const QJsonObject &json);
	void openWebsocket();
signals:
	void clientMessage(const QString &sender, const QJsonObject &data);
	void clientAdded(const QString &uuid);
	void clientRemoved(const QString &uuid);
	void error(const QString &msg);
	void connected();
};

#endif // WEBSOCKETCLIENT_H
