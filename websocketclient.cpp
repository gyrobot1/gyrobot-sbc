#include "websocketclient.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

enum MessageType{
	ClientAdded = 1,
	ClientRemoved = 2,
	Init = 3,
	SendAll = 4,
	SendTo = 5,
	Error = 6,
	ChangeSector = 7,
	CreateSector = 8,
	CloseSector = 9,
	Kick = 10,
	AccessDenied = 11
};

void WebSocketClient::handleClientRemoved(const QJsonObject &json){
	QString id = json["id"].toString();
	int i = m_clients.indexOf(id);
	if (i!=-1) {
		m_clients.removeAt(i);
		emit clientRemoved(id);
	}	else qInfo() << "index not found";
	//qInfo() << "after removed" << m_clients;
}

void WebSocketClient::handleClientAdded(const QJsonObject &json){
	QString id = json["id"].toString();
	if(id.compare(m_id)!=0){
		m_clients.append(id);
		emit clientAdded(id);
	}
	//qInfo() << "afteradded" << m_clients;
}

void WebSocketClient::handleInit(const QJsonObject &json){
	m_id = json["id"].toString();
	//qInfo() << "init: "<< m_id;
	m_clients.clear();
	QJsonArray clients = json["clients"].toArray();
	for (auto client : clients){
		QString id = client.toString();
		if (id.compare(m_id)!=0) m_clients.append(id);
	}
}


void WebSocketClient::handleClientMessage(const QJsonObject &json){
	const QJsonObject data = json["d"].toObject();
	const QString sender = json["sender"].toString();
	if (sender.compare(m_id)!=0) emit clientMessage(sender, data);
}

void WebSocketClient::handleError(const QJsonObject &json)
{
	const QString msg = json["msg"].toString();
	emit error(msg);
}

void WebSocketClient::handleMessage(const QJsonObject &json)
{
	MessageType type = (MessageType)json["t"].toInt();
	switch (type) {
	case MessageType::Init:
		handleInit(json);
		break;
	case MessageType::ClientAdded:
		handleClientAdded(json);
		break;
	case MessageType::ClientRemoved:
		handleClientRemoved(json);
		break;
	case MessageType::SendAll:
	case MessageType::SendTo:
		handleClientMessage(json);
		break;
	case MessageType::Error:
		handleError(json);
		break;
	default:
		qInfo() << "message not handled";
		qInfo() << json;
	}
}

WebSocketClient::WebSocketClient(const QUrl &url, bool debug, QObject *parent)
	: QObject{parent},m_url(url),m_debug(debug)
{
	if (m_debug)
		qInfo() << "WebSocket server:" << url;
	connect(&m_webSocket, SIGNAL(connected()), this, SLOT(onConnected()));
	connect(&m_webSocket, SIGNAL(disconnected()), this, SLOT(onClosed()));
	connect(&m_webSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
	connect(&m_pingTimer, SIGNAL(timeout()), this, SLOT(onPingTimer()));
	openWebsocket();
}

void WebSocketClient::openWebsocket(){
	m_webSocket.open(QUrl(m_url));
	m_pingTimer.start(5000);
}

void WebSocketClient::sendAll(const QJsonObject &data)
{
	QJsonObject obj;
	obj["t"] = (int)MessageType::SendAll;
	obj["d"] = data;
	QJsonDocument doc(obj);
	sendMessage(doc.toJson(QJsonDocument::Compact));
}

void WebSocketClient::sendTo(const QString &id, const QJsonObject &data){
	QJsonObject obj;
	obj["t"] = (int)MessageType::SendTo;
	obj["id"] = id;
	obj["d"] = data;
	QJsonDocument doc(obj);
	sendMessage(doc.toJson(QJsonDocument::Compact));
}

void WebSocketClient::changeSector(const QString &id)
{
	QJsonObject obj;
	obj["t"] = (int)MessageType::ChangeSector;
	obj["id"] = id;
	QJsonDocument doc(obj);
	sendMessage(doc.toJson(QJsonDocument::Compact));
}

void WebSocketClient::createSector(const QString &sectorId, const QString &hostId, const QString &hostToken)
{
	QJsonObject obj;
	obj["t"] = (int)MessageType::CreateSector;
	obj["id"] = sectorId;
	obj["hostToken"] = hostToken;
	obj["hostId"] = hostId;
	QJsonDocument doc(obj);
	sendMessage(doc.toJson(QJsonDocument::Compact));
}

void WebSocketClient::sendMessage(QString msg)
{
	m_webSocket.sendTextMessage(msg);
}

void WebSocketClient::onConnected()
{
	qInfo() << "on ws Connected";
	connect(&m_webSocket, SIGNAL(textMessageReceived(QString)),
							this, SLOT(onTextMessageReceived(QString)));
	emit connected();
}

void WebSocketClient::onTextMessageReceived(QString msg)
{
	//if (m_debug) qInfo() << "Recieved message: " << msg;
	QJsonDocument doc = QJsonDocument::fromJson(msg.toUtf8());
	if (!doc.isEmpty()){
		handleMessage(doc.object());
	} else {
		qInfo()<<msg;
	}
}

void WebSocketClient::onClosed()
{
	m_pingTimer.stop();
	qInfo() << "WebSocketClient closed";
	openWebsocket();
}

void WebSocketClient::onError(QAbstractSocket::SocketError error)
{
	qInfo() << "WebSocketClient error: " << error;
}

void WebSocketClient::onPingTimer()
{
	m_webSocket.ping(); // this keeps QWebSocket alive
}
