#ifndef GYROBOTAI_H
#define GYROBOTAI_H

#include <QObject>
#include <QQuaternion>
#include "pidregulator.h"
#include "serialhandler.h"

enum AIState{
	Calibrating,
	Balancing
};

class GyrobotAI : public QObject
{
	Q_OBJECT
	SensorData m_sensor_data;
	PIDRegulator m_balance_regulator;
	PIDRegulator m_speed_regulator;
	QVector3D m_acceleration = QVector3D(0,-1,0);
	QVector3D m_gyro = QVector3D(0,0,0);
	QVector3D m_gyro_offset = QVector3D(0,0,0);
	AIState m_state = AIState::Calibrating;
    double m_pivotAngle = 0.02;
	double m_wheelCircomference = 0.535;
	double m_dataFrequency = 100.0;
	double m_pulsesPerRotation = 200.0;
	double m_currentSpeed;
	double m_pulseSpeed;
	double m_accelSpeed;
	double m_position_z;

	QQuaternion m_orientation;
	double m_balance_offset = 0;
	double m_delta_time = 0.005;
public:
	GyrobotAI(QObject *parent);
private:
	void regulate();
public slots:
	void onInfo(QString info);
	void onError(QString error);
	void onSensorData(SensorData sensor_data);

signals:
	void regulation(double left, double right);
private slots:
	void onBalanceTime();
};

#endif // GYROBOTAI_H
